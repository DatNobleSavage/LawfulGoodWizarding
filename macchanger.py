#!/usr/bin/env python

import subprocess

lan_interface = input("[+] Interface to change:    ")
new_mac = input("[+] New MAC address:    ")

print('(")> Changing MAC for', lan_interface, "to", new_mac)

subprocess.run(['ifconfig', lan_interface, 'down'])
subprocess.run(['ifconfig', lan_interface, 'hw', 'ether', new_mac])
subprocess.run(['ifconfig', lan_interface, 'up'])
subprocess.run(['ifconfig', lan_interface])
