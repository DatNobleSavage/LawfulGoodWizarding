#!/usr/bin/env python

import subprocess

lan_interface = input("[+] Interface to change:    ")


print("[+] Resetting", lan_interface, "to default.")

subprocess.call("ifconfig wlan0 down", shell=True)
subprocess.call("ifconfig wlan0 hw ether $(ethtool -P wlan0 | awk '{print $3}')", shell=True)
subprocess.call("ifconfig wlan0 up", shell=True)
subprocess.call("ifconfig", shell=True)
